<?php

namespace Drupal\weather_widget\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\weather_widget\WeatherService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WeatherLocationForm extends FormBase {

  /**
   * The weather service.
   *
   * @var \Drupal\weather_widget\WeatherService
   */
  protected $weatherService;

  /**
   * Constructs a WeatherLocationForm object.
   *
   * @param \Drupal\weather_widget\WeatherService $weather_service
   *   The weather service.
   */
  public function __construct(WeatherService $weather_service) {
    $this->weatherService = $weather_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('weather_widget.weather_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_location_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#description' => $this->t('Enter a city or location.'),
      '#default_value' => \Drupal::request()->query->get('location'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::updateWeather',
        'wrapper' => 'weather-widget-wrapper',
        'method' => 'replace',
        'event' => 'change',
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Get Weather'),
      '#ajax' => [
        'callback' => '::updateWeather',
        'wrapper' => 'weather-widget-wrapper',
      ],
    ];

    $form['weather_output'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'weather-widget-wrapper'],
    ];

    return $form;
  }

  /**
   * AJAX callback for the location change.
   */
  public function updateWeather(array $form, FormStateInterface $form_state) {
    $location = $form_state->getValue('location');
    $weather = $this->weatherService->fetchWeather($location);

    $form['weather_output']['#markup'] = $weather['temperature'] . '°C - ' . $weather['description'];
    return $form['weather_output'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $location = $form_state->getValue('location');
    if (empty($location)) {
        $form_state->setErrorByName('location', $this->t('Please enter a valid location.'));
    }
    // Additional validation checks can be added here.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This function can remain empty, as the AJAX callback is now handling the main functionality.
  }
}
