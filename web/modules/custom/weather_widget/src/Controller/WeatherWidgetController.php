<?php

namespace Drupal\weather_widget\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Cache\CacheBackendInterface;

class WeatherWidgetController extends ControllerBase {

  /**
   * Config factory instance.
   */
  protected $configFactory;

  /**
   * Http client instance.
   */
  protected $httpClient;

  /**
   * Cache instance
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, CacheBackendInterface $cache) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->cache = $cache;
  }

  /**
   * Fetch needed data.
   */
  public function fetchWeather($location = NULL) {
    // Check cache first
    if ($cache = $this->cache->get('weather_widget.data')) {
      return $cache->data;
    }

    $location = $location ?: $this->getUserLocation();
    $api_key = $this->configFactory->get('weather_widget.settings')->get('api_key');
    $response = $this->httpClient->request('GET', "http://api.openweathermap.org/data/2.5/weather", [
      'query' => [
        'lat' => $location['latitude'],
        'lon' => $location['longitude'],
        'appid' => $api_key
      ]
    ]);

    $weather_data = json_decode($response->getBody());
    $weather = [
      'temperature' => $weather_data->main->temp,
      'description' => $weather_data->weather[0]->description,
    ];

    // Cache the data for 1 hour.
    $this->cache->set('weather_widget.data', $weather, time() + 3600);

    return $weather;
  }

  /**
   * AJAX callback to provide the latest weather data in JSON format.
   */
  public function ajaxRefresh() {
    // Fetch the current weather data.
    $weather = $this->fetchWeather();
  
    // Return the temperature and description as a JSON response.
    return new JsonResponse([
      'temperature' => $weather['temperature'],
      'description' => $weather['description'],
    ]);
  }

  /**
   * Gets user's location.
   */
  private function getUserLocation() {
    // Get user's location. Fully integrate with the Geolocation Field module.
    return ['latitude' => '50.4501', 'longitude' => '30.5234'];  // Default: Kyiv.
  }
}
