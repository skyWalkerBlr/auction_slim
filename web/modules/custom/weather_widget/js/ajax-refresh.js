(function ($, Drupal) {
    Drupal.behaviors.weatherWidgetRefresh = {
      attach: function (context, settings) {
        // Reference to the weather widget block.
        let $widget = $('.weather-widget', context);
        
        // Set an interval to perform the AJAX request and update the widget.
        setInterval(() => {
          $.ajax({
            // The URL endpoint for fetching the latest weather data.
            url: '/weather-widget/ajax-refresh',
            method: 'GET',
            success: function (data) {
              // Update the widget with the new temperature and description.
              if (data.temperature && data.description) {
                $widget.find('.temperature').text(data.temperature + '°C');
                $widget.find('.description').text(data.description);
              }
            }
          });
        }, 60000); // Refresh rate is set to 1 minute.
      }
    };
  })(jQuery, Drupal);
  