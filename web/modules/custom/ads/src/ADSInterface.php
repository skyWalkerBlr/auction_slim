<?php

declare(strict_types = 1);

namespace Drupal\ads;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an ads entity type.
 */
interface ADSInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
