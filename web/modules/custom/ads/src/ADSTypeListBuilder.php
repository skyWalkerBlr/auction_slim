<?php

declare(strict_types = 1);

namespace Drupal\ads;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of ads type entities.
 *
 * @see \Drupal\ads\Entity\ADSType
 */
final class ADSTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No ads types available. <a href=":link">Add ads type</a>.',
      [':link' => Url::fromRoute('entity.ads_type.add_form')->toString()],
    );

    return $build;
  }

}
