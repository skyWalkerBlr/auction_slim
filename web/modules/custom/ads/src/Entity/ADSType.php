<?php

declare(strict_types = 1);

namespace Drupal\ads\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the ADS type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "ads_type",
 *   label = @Translation("ADS type"),
 *   label_collection = @Translation("ADS types"),
 *   label_singular = @Translation("ads type"),
 *   label_plural = @Translation("adss types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count adss type",
 *     plural = "@count adss types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\ads\Form\ADSTypeForm",
 *       "edit" = "Drupal\ads\Form\ADSTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\ads\ADSTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer ads types",
 *   bundle_of = "ads",
 *   config_prefix = "ads_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/ads_types/add",
 *     "edit-form" = "/admin/structure/ads_types/manage/{ads_type}",
 *     "delete-form" = "/admin/structure/ads_types/manage/{ads_type}/delete",
 *     "collection" = "/admin/structure/ads_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class ADSType extends ConfigEntityBundleBase {

  /**
   * The machine name of this ads type.
   */
  protected string $id;

  /**
   * The human-readable name of the ads type.
   */
  protected string $label;

}
